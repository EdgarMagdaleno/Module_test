#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include "declarations.h"

static struct file_operations main_fops = {
	.read = main_read,
	.write = main_write,
	.open = main_open,
	.release = main_release
};

static struct file_operations secondary_fops = {
	.read = secondary_read,
	.write = secondary_write,
	.open = secondary_open,
	.release = secondary_release
};

static int
__init module_start(void) {
	// Initilization of Write device
	main_major_number = register_chrdev(0, MAIN_NAME, &main_fops);

	if(main_major_number < 0) {
		printk(KERN_ALERT "Write device: Registering char device failed with %d\n", main_major_number);
		return main_major_number;
	}

	main_class_p = class_create(THIS_MODULE, MAIN_CLASS);
	main_device_p = device_create(main_class_p, NULL, MKDEV(main_major_number, 0), NULL, MAIN_NAME);

	printk(KERN_INFO "Write device: Registered succesfully with major number %i.\n", main_major_number);

	// Initilization of secondary device
	secondary_major_number = register_chrdev(0, SECONDARY_NAME, &secondary_fops);

	if(secondary_major_number < 0) {
		printk(KERN_ALERT "Registering char device failed with %d\n", secondary_major_number);
		return secondary_major_number;
	}

	secondary_class_p = class_create(THIS_MODULE, SECONDARY_CLASS);
	secondary_device_p = device_create(secondary_class_p, NULL, MKDEV(secondary_major_number, 0), NULL, SECONDARY_NAME);

	printk(KERN_INFO "Read device: registered succesfully with major number %i.\n", secondary_major_number);
	return 0;
}

static void
__exit module_clean(void) {
	device_destroy(main_class_p, MKDEV(main_major_number, 0));
	class_unregister(main_class_p);
	class_destroy(main_class_p);
	unregister_chrdev(main_major_number, MAIN_NAME);
	printk(KERN_INFO "Write device: Closed.");

	device_destroy(secondary_class_p, MKDEV(secondary_major_number, 0));
	class_unregister(secondary_class_p);
	class_destroy(secondary_class_p);
	unregister_chrdev(secondary_major_number, SECONDARY_NAME);
	printk(KERN_INFO "Read device: Closed.");
}

// Start of main file operations.
static int
main_open(struct inode *inode, struct file *filp) {
	printk(KERN_INFO "Write device: Opened.");
	try_module_get(THIS_MODULE);
	return 0;
}

static int
main_release(struct inode *inode, struct file *filp) {
	printk(KERN_INFO "Write device: Released.");
	module_put(THIS_MODULE);
	return 0;
}

static ssize_t
main_read(struct file *file_p, char *user_buffer, size_t length, loff_t *offset) {
	printk(KERN_ALERT "Write device: Reading prohibited.");
	return -EPERM;
}

static ssize_t
main_write(struct file *file_p, const char *message, size_t length, loff_t *offset) {
	ssize_t bytes = length < BUFFER_LENGTH ? length : BUFFER_LENGTH;

	int count = 0;
	while(count < bytes) {
		buffer[count++] = *(message)++;
	}

	printk(KERN_INFO "Write device: Received %zu bytes from the user.", bytes);
	(*offset) += bytes;
	buffer_fill = bytes;
	return bytes;
}
// End of main file operations.

// Start of secondary file operations.
static int
secondary_open(struct inode *inode, struct file *filp) {
	printk(KERN_INFO "Read device: Opened.");
	try_module_get(THIS_MODULE);
	return 0;
}

static int
secondary_release(struct inode *inode, struct file *filp) {
	printk(KERN_INFO "Read device: Released.");
	module_put(THIS_MODULE);
	return 0;
}

static ssize_t
secondary_read(struct file *file_p, char *user_buffer, size_t length, loff_t *offset) {
	ssize_t bytes = length < (buffer_fill - (*offset)) ? length : (buffer_fill - (*offset));

	if(copy_to_user(user_buffer, buffer, bytes)) {
		return -EFAULT;
	}

	printk(KERN_INFO "Read device: Sent %zu bytes to the user.", bytes);
	(*offset) += bytes;
	return bytes;
}

static ssize_t
secondary_write(struct file *file_p, const char *message, size_t length, loff_t *offset) {
	printk(KERN_ALERT "Read device: Writting is prohibited.");
	return -EPERM;
}
// End of secondary file operations.

module_init(module_start);
module_exit(module_clean);
MODULE_LICENSE("GPL");