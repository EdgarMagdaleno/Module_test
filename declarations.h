#define MAIN_NAME "write_device"
#define MAIN_CLASS "write_class"
#define SECONDARY_NAME "read_device"
#define SECONDARY_CLASS "read_class"
#define BUFFER_LENGTH 8

static int main_open(struct inode *, struct file *);
static int main_release(struct inode *, struct file *);
static ssize_t main_read(struct file *, char *, size_t, loff_t *);
static ssize_t main_write(struct file *, const char *, size_t, loff_t *);

static int secondary_open(struct inode *, struct file *);
static int secondary_release(struct inode *, struct file *);
static ssize_t secondary_read(struct file *, char *, size_t, loff_t *);
static ssize_t secondary_write(struct file *, const char *, size_t, loff_t *);

static char buffer[BUFFER_LENGTH];
static size_t buffer_fill = 0;
static int main_major_number;
static int secondary_major_number;
static struct class* main_class_p = NULL;
static struct device* main_device_p = NULL;
static struct class* secondary_class_p = NULL;
static struct device* secondary_device_p = NULL;